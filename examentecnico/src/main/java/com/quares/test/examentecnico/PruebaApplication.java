package com.quares.test.examentecnico;

import com.quares.test.examentecnico.health.TemplateHealthCheck;
import com.quares.test.examentecnico.resources.MovieResource;
import com.quares.test.examentecnico.api.MovieAPIService;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class PruebaApplication extends Application<PruebaConfiguration> {

	public static void main(String[] args) throws Exception {
        new PruebaApplication().run(args);
    }
   
    @Override
    public void run(PruebaConfiguration configuration,
                    Environment environment) {
    	final MovieResource resource = new MovieResource(
	        configuration.getApiUri(),
	        configuration.getApiKey()
   	    );
	    environment.jersey().register(resource);
    }

}
