package com.quares.test.examentecnico.core;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MovieRepresentation {
	private String APIResult;
	
    public MovieRepresentation() {
        // Jackson deserialization
    }

    public MovieRepresentation(String result) {
    	this.APIResult = result;
    }

    @JsonProperty
    public String getAPIResult() {
        return APIResult;
    }
}