package com.quares.test.examentecnico.resources;

import java.util.concurrent.atomic.AtomicLong;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import com.codahale.metrics.annotation.Timed;
import com.quares.test.examentecnico.api.MovieAPIService;
import com.quares.test.examentecnico.core.MovieRepresentation;

import io.dropwizard.logback.shaded.guava.base.Optional;



@Path("/pelis")
@Produces(MediaType.APPLICATION_JSON)
public class MovieResource {

	private Client client = ClientBuilder.newClient();
	private String apiUri;
	private String apiKey;
	
    public MovieResource(String apiUri, String apiKey) {
    	this.apiUri = apiUri;
        this.apiKey = apiKey;
    }
    
    @GET
    @Timed
    @Path("/search")
    public MovieRepresentation searchMovies(@QueryParam("title") String title, @QueryParam("type") String type, @QueryParam("year") String year, @QueryParam("page") String page) {
    	String result = listMovies(title,type,year, page);
    	return new MovieRepresentation(result);
    }
    
    private String listMovies(String title, String type, String year, String page) {
    	String response = "";
    	if(title != null && type != null && year != null) {
    		UriBuilder uri = UriBuilder.fromPath(apiUri).queryParam("apikey", apiKey).queryParam("s", title).queryParam("type", type).queryParam("y", year).queryParam(page, page);
    		Response result = client.target(uri).request().get();
    		if(result.getStatus() != 200) {
    			response = "Error en la coneccion con la api";
    		}else {
    			response = result.readEntity(String.class);
    		}
    	}else {
    		response = "No especifica tipo de busqueda."; 
    	}
    	return response;
	}
	
    @GET
    @Timed
    @Path("/search")
    public MovieRepresentation getMovie(@QueryParam("title") String title) {
    	String result = getMovieByTitle(title);
    	return new MovieRepresentation(result);
	}
    
    private String getMovieByTitle(String title) {
    	String response = "";
    	if(title != null) {
    		UriBuilder uri = UriBuilder.fromPath(apiUri).queryParam("apikey", apiKey).queryParam("t", title);
    		Response result = client.target(uri).request().get();
    		if(result.getStatus() != 200) {
    			response = "Error en la coneccion con la api";
    		}else {
    			response = result.readEntity(String.class);
    		}
    	}else {
    		response = "No especifica titulo para la busqueda."; 
    	}
    	return response;
	}
}