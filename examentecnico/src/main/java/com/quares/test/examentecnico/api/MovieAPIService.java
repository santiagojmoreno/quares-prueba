package com.quares.test.examentecnico.api;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.UriBuilder;

public class MovieAPIService {
	private Client client = ClientBuilder.newClient();
	private String apiUri = "http://www.omdbapi.com/";
	private String apiKey = "37f5a6f6";
	
	public MovieAPIService() {
		
	}
	
	public String listMovies(String title, String type, String year) {
		String result = client.target(apiUri + "?apikey=" + apiKey + "&s=" + title).request().get(String.class);
		return result;
	}
	
	public String getMovie(String title, String type, String year) {
		String result = client.target("http://www.omdbapi.com/?apikey=37f5a6f6&s=Future").request().get(String.class);
		return result;
	}
}
