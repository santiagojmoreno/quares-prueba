package com.quares.test.examentecnico;

import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotEmpty;;

public class PruebaConfiguration extends Configuration {
	@NotEmpty
    private String apiUri;

    @NotEmpty
    private String apiKey;

    @JsonProperty
    public String getApiUri() {
        return apiUri;
    }

    @JsonProperty
    public void setApiUri(String apiUri) {
        this.apiUri = apiUri;
    }

    @JsonProperty
    public String getApiKey() {
        return apiKey;
    }

    @JsonProperty
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
